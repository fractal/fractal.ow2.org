// START SNIPPET: Content

/**
 * Minus: a component library for the construction of operating systems.
 *        Compliant with the Fractal/Cecilia component model.
 *
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WACCANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: think@objectweb.org
 *
 * Authors: Germain Haugou (STMicroelectronics).
 *
 */

DECLARE_DATA {};

#include <cecilia.h>

void METHOD(entry, main) (void *_this, jint argc, char **argv){
  printf("Hello\nWorld !\n");
  while (1);
}
