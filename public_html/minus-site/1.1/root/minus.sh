
# ============================
#      Minus configuration
# ============================
	
export MINUS_VERSION=TO_BE_MODIFIED

# For targets using the X server
export X11_INC_PATH=/home/ecombase/SDS/Linux/WS3/X11R6/include
export X11_LIB_PATH=/home/ecombase/SDS/Linux/WS3/X11R6/lib

# For ST200 targets
export ST200_COMPILER_PATH=/home/ecombase/tools/i686-pc-linux-gnu/ST200/Open64/st200_R6.1

# For the Lxsimenv platform
export ASTISS_ST231_PATH=/home/ecombase/tools/i686-pc-linux-gnu/ST200/simenv-mt_snapshot_080107/st231

# For the Traviata platform
export ST200_STMC_PATH=/home/ecombase/tools/i686-pc-linux-gnu/ST200/Open64/st200_R5.0-20060110_Micro_toolset
export STM8010_STMC_IP=164.129.123.233

# For the xSTream platform
export PTHREAD_LIB_PATH=/home/ecombase/tools/i686-pc-linux-gnu/pth-2.0.7/lib
export XSTREAM_PACKAGE_PATH=/home/ecombase/tools/i686-pc-linux-gnu/XSTREAM/XSTREAM080306

# For ARM targets
export ARM_COMPILER_PATH=/home/ecombase/tools/i686-pc-linux-gnu/ARM/arm-2005q3-1
export ARM_GCC_VERSION=3.4.4

# For the Integrator platform
export QEMU_PATH=/home/ecombase/tools/i686-pc-linux-gnu/ARM
